package com.doer.mraims.address.adapter.out.persistance.database.repository;


import com.doer.mraims.address.adapter.out.persistance.database.entity.AddressEntity;
import com.doer.mraims.core.util.Table;
import lombok.AllArgsConstructor;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;
import java.util.Map;

@AllArgsConstructor
@Repository
public class AddressRepositoryImpl implements AddressRepository {

    private final JdbcTemplate jdbcTemplate;


    @Override
    public int countAddress(String schema) {
        String sql = "Select count(*) from " + schema  + Table.ADDRESS;
        return jdbcTemplate.queryForObject(sql, Integer.class);
    }

    @Override
    public int countTotalAddressForPagination(Map<String, String> params) {
        return 0;
    }

    @Override
    public List<AddressEntity> findAllAddress(Map<String, String> params) {
        return null;
    }

    @Override
    public AddressEntity findByAddressId(String branchId, Map<String, String> params) {
        return null;
    }

    @Override
    public AddressEntity findByOid(String oid, Map<String, String> params) {
        AddressEntity data = new AddressEntity();

        try {
            String sql = "SELECT oid, address_id, address_name_en, address_name_bn, address_code, status, area_oid, "
                    + " created_by, created_on, updated_by, updated_on "
                    + " from " + params.get("schemaName") + Table.ADDRESS + " br "
                    + " where 1 = 1 and  br.oid = '" + oid + "'";

            List<Map<String, Object>> rows = jdbcTemplate.queryForList(sql);

            for (Map<String, Object> row : rows) {
                data.setOid((String) row.get("oid"));
                data.setAddressId((String) row.get("address_id"));
                data.setAddressNameEn((String) row.get("address_name_en"));
                data.setAddressNameBn((String) row.get("address_name_bn"));
                data.setAddressCode((String) row.get("address_code"));
                data.setStatus((String) row.get("status"));
                data.setAreaOid((String) row.get("area_oid"));

                data.setCreatedBy((String) row.get("created_by"));
                data.setCreatedOn((Date) row.get("created_on"));
                data.setUpdatedBy((String) row.get("updated_by"));
                data.setUpdatedOn((Date) row.get("updated_on"));

                break;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return data;
    }

    @Override
    public int saveWithSchema(AddressEntity entity, Map<String, String> params) {
        String sql = "INSERT INTO " + params.get("schemaName") + Table.ADDRESS + "(" +
                " oid, address_id, address_name_en, address_name_bn, address_code, status, area_oid, created_by )" +
                " VALUES (?, ?, ?, ?, ?, ?, ?, ?)";

        return jdbcTemplate.update(sql, entity.getOid(), entity.getAddressId(), entity.getAddressNameEn(),
                entity.getAddressNameBn(), entity.getAddressCode(), entity.getStatus(), entity.getAreaOid(), entity.getCreatedBy());
    }

    @Override
    public int updateWithSchema(AddressEntity entity, Map<String, String> params) {
        try {
            String sql = "update " + params.get("schemaName") + Table.ADDRESS
                    + " set address_name_en = ?, address_name_bn = ?,"
                    + " address_code = ?, status = ?, updated_by = ?, updated_on = current_timestamp  where oid = ?";

            return jdbcTemplate.update(sql, entity.getAddressNameEn(),
                    entity.getAddressNameBn(), entity.getAddressCode(), entity.getStatus(),
                    entity.getUpdatedBy(), entity.getOid());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }
}
