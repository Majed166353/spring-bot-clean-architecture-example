package com.doer.mraims.area.adapter.out.persistance.database.repository;

import com.doer.mraims.core.util.Table;
import com.doer.mraims.core.util.exception.ExceptionHandlerUtil;
import com.doer.mraims.area.adapter.out.persistance.database.entity.AreaEntity;
import lombok.AllArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

@AllArgsConstructor
@Repository
public class AreaRepositoryImpl implements AreaRepository{

    private final JdbcTemplate jdbcTemplate;

    @Override
    public int count(String schema) {
        String sql = "Select count(*) from " + schema + Table.AREA;
        return jdbcTemplate.queryForObject(sql, Integer.class);
    }

    @Override
    public int saveArea(AreaEntity entity, Map<String, String> params) throws ExceptionHandlerUtil {
        String sql = "INSERT INTO " + params.get("schemaName") + Table.AREA + "(" +
                " oid, area_id, name_en, name_bn, status, created_by )" +
                " VALUES (?, ?, ?, ?, ?, ?)";

        return jdbcTemplate.update(sql, entity.getOid(), entity.getAreaId(), entity.getNameEn(),
                entity.getNameBn(), entity.getStatus(), entity.getCreatedBy());
    }

    @Override
    public int countTotalAreaForPagination(Map<String, String> params) {
        String sql = "SELECT count(ar.oid)"
                + " from " + params.get("schemaName") + Table.AREA + " ar "
                + " where 1 = 1 ";

        if (StringUtils.isNotBlank(params.get("status"))) {
            sql += " and ar.status  =  '" + params.get("status") + "' ";
        }

        if (StringUtils.isNotBlank(params.get("searchText"))) {
            sql += " and (ar.area_id ilike '%" + params.get("searchText").trim() + "%'"
                    + " or ar.area_code ilike '%" + params.get("searchText").trim() + "%'"
                    + " or ar.area_name_en ilike '%" + params.get("searchText").trim() + "%'"
                    + " or ar.area_name_bn ilike '%" + params.get("searchText").trim() + "%'"
                    + " or ar.zone_id ilike '%" + params.get("searchText").trim() + "%'"
                    + " or ar.status ilike '%" + params.get("searchText").trim() + "%')";
        }
        return jdbcTemplate.queryForObject(sql, Integer.class);
    }

    @Override
    public List<AreaEntity> findAllArea(Map<String, String> params) {
        List<AreaEntity> dataList = new ArrayList<>();

        String sql = "SELECT ar.oid, ar.area_id, ar.area_code, ar.area_name_en, ar.area_name_bn, "
                + " ar.status, ar.zone_id, ar.created_by, ar.created_on, z.zone_name_en, z.zone_name_bn"
                + " from " + params.get("schemaName") + Table.AREA + " ar "
                + " left join " + params.get("schemaName") + Table.ZONE + " z on z.zone_id = ar.zone_id"
                + " where 1 = 1 ";

        if (StringUtils.isNotBlank(params.get("status"))) {
            sql += " and ar.status  =  '" + params.get("status") + "' ";
        }
        if (StringUtils.isNotBlank(params.get("searchText"))) {
            sql += " and (ar.area_id ilike '%" + params.get("searchText").trim() + "%'"
                    + " or ar.area_code ilike '%" + params.get("searchText").trim() + "%'"
                    + " or ar.area_name_en ilike '%" + params.get("searchText").trim() + "%'"
                    + " or ar.area_name_bn ilike '%" + params.get("searchText").trim() + "%'"
                    + " or ar.zone_id ilike '%" + params.get("searchText").trim() + "%'"
                    + " or ar.status ilike '%" + params.get("searchText").trim() + "%')";
        }

        if (StringUtils.isNotBlank(params.get("sortColId")) && StringUtils.isNotBlank(params.get("sortOrder"))) {
            sql += " order by ar." + params.get("sortColId") + " " + params.get("sortOrder");
        } else {
            sql += " order by ar.created_on asc";
        }
        if (Integer.valueOf(params.get("offset")) >= 0) {
            sql += " offset " + params.get("offset");
        }
        if (Integer.valueOf(params.get("limit")) >= 0) {
            sql += " limit " + params.get("limit");
        }

        List<Map<String, Object>> rows = jdbcTemplate.queryForList(sql);
        for (Map<String, Object> row : rows) {
            AreaEntity data = new AreaEntity();
            data.setOid((String) row.get("oid"));
            data.setAreaId((String) row.get("area_id"));
            data.setAreaCode((String) row.get("area_code"));
            data.setNameBn((String) row.get("area_name_en"));
            data.setNameEn((String) row.get("area_name_bn"));
            data.setNameBn((String) row.get("zone_id"));
            data.setStatus((String) row.get("status"));
            data.setCreatedBy((String) row.get("created_by"));
            data.setCreatedOn((Date) row.get("created_on"));

            data.setZoneNameEn((String) row.get("zone_name_en"));
            data.setZoneNameBn((String) row.get("zone_name_bn"));

            dataList.add(data);
        }

        return dataList;
    }

    @Override
    public AreaEntity findByOid(String oid, Map<String, String> params) {
        AreaEntity data = new AreaEntity();

        try {
            String sql = "SELECT ar.oid, ar.area_id, ar.name_en, ar.name_bn, ar.status, "
                    + " ar.created_by, ar.created_on, ar.updated_by, ar.updated_on "
                    + " from " + params.get("schemaName") + Table.AREA + " ar "
                    + " where 1 = 1 and  ar.oid = '" + oid + "'";

            List<Map<String, Object>> rows = jdbcTemplate.queryForList(sql);

            for (Map<String, Object> row : rows) {
                data.setOid((String) row.get("oid"));
                data.setAreaId((String) row.get("area_id"));
                data.setNameEn((String) row.get("name_en"));
                data.setNameBn((String) row.get("name_bn"));
                data.setStatus((String) row.get("status"));
                data.setCreatedBy((String) row.get("created_by"));
                data.setCreatedOn((Date) row.get("created_on"));
                data.setUpdatedBy((String) row.get("updated_by"));
                data.setUpdatedOn((Date) row.get("updated_on"));
                break;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return data;
    }

    @Override
    public int updateArea(AreaEntity entity, Map<String, String> params) {
        try {
            String sql = "update " + params.get("schemaName") + Table.AREA
                    + " set name_en = ?, name_bn = ?, status = ?, "
                    + " updated_by = ?, updated_on = current_timestamp  where oid = ?";
            return jdbcTemplate.update(sql, entity.getNameEn(),
                    entity.getNameBn(), entity.getStatus(),
                    entity.getUpdatedBy(), entity.getOid());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }
}
